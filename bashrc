export VISUAL=vim
export EDITOR="$VISUAL"
#export PS1="\u@\h \[\033[32m\]\w\[\033[33m\]\$(parse_git_branch)\[\033[00m\] $ "
shopt -s histappend;
shopt -s cdspell;
shopt -s nocaseglob;

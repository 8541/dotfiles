local utils = require("mp.utils")

mp.observe_property("audio-device-list", "native", function(name, val)
	--print("Audio device list changed:")
	for index, e in ipairs(val) do
		--print("  - '" .. e.name .. "' (" .. e.description .. ")")
		if e.name:sub(1, #'pulse/bluez_sink') == 'pulse/bluez_sink' then
			mp.set_property("audio-device",e.name)
			mp.osd_message("audio="..e.name)
		end
	end
end)

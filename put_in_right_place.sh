#!/usr/bin/env bash
mkdir -p ~/.config/i3
mkdir -p ~/.config/i3blocks
mkdir -p ~/.config/mpv/scripts
mkdir -p ~/.config/ranger
cp -i i3-config       ~/.config/i3/config
cp -i i3blocks-config ~/.config/i3blocks/config
cp -i mpv.conf        ~/.config/mpv/
cp -i use_bluetooth.lua ~/.config/mpv/scripts/
cp -i psqlrc          ~/.psqlrc
cp -i rc.conf         ~/.config/ranger/
cp -i rifle.conf      ~/.config/ranger/
cp -i tmux.conf       ~/.tmux.conf
cp -i Xresources      ~/.Xresources
cat bashrc >> ~/.bashrc
cat /etc/inputrc inputrc > ~/.inputrc
